import pytest
from fixture.app_manager import AppManager

fixture = None
#tes111

@pytest.fixture(scope='module')
def app(request):
    global fixture
    if fixture is None:
        fixture = AppManager(1)

    prepeare_to_test(fixture)
    return fixture

def prepeare_to_test(fixture):
    posts = fixture.gen.get_objects(fixture)
    for post in posts:
        fixture.gen.delete_objects(fixture, id=post['id'])