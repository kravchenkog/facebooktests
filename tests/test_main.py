import pytest
import random
import time

class TestCreateProject:
    @pytest.fixture(autouse=True)
    def _streamer(self, app):
        self.app = app
# test yocto
    def test_WHEN_new_post_EXPECTED_the_number_of_posts_is_increased(self):

        app = self.app
        data_len_before = len(app.gen.get_objects(app))
        message = app.stg_gen.get_random_string(num=100)
        app.gen.post_object(app, message)
        data_len_after = len(app.gen.get_objects(app))
        assert data_len_before + 1 == data_len_after

    def test_WHEN_new_post_EXPECTED_an_id_of_last_post_is_proper(self):
        app = self.app
        message = app.stg_gen.get_random_string(num=100)
        id = app.gen.post_object(app, message)['id']
        id_get = app.gen.get_objects(app)[0]['id']
        assert id == id_get

    def test_WHEN_new_post_EXPECTED_the_text_of_post_is_proper(self):
        app = self.app
        message = app.stg_gen.get_random_string(num=100)
        app.gen.post_object(app, message)
        mes_get = app.gen.get_objects(app)[0]['message']
        assert message == mes_get

    def test_WHEN_post_is_deleted_EXPECTED_the_number_of_posts_is_decreased(self):
        app = self.app
        data = app.gen.get_objects(app)
        data_len_before = len(data)
        id = random.choice(data)['id']
        resp  = app.gen.delete_objects(app, id)
        print(resp)
        time.sleep(1)
        data_len_after = len(app.gen.get_objects(app))
        assert data_len_before - 1 == data_len_after

    def test_WHEN_post_is_deleted_EXPECTED_id_is_not_presented(self):
        app = self.app
        data = app.gen.get_objects(app)
        id = random.choice(data)['id']
        app.gen.delete_objects(app, id)
        data_ids = [x['id'] for x in app.gen.get_objects(app)]
        assert id not in data_ids

    def test_WHEN_repost_is_posted_EXPECTED_repost_is_published(self):
        app = self.app
        message = app.stg_gen.get_random_string(num=random.randint(100, 300))
        data = app.gen.get_objects(app)
        id = random.choice(data)['id'].split('_')[1]
        repost_id = app.gen.repost_object(app, message, id)['id']

        repost_data = app.gen.get_objects(app)

        assert repost_id == repost_data[0]['id']



