import json
import facebook


class GeneralHelper:

    def post_object(self, app, message):
        '''
        this method create a post
        '''
        graph = facebook.GraphAPI(access_token=app.env.token)

        resp = graph.put_object("me", "feed", message=message)
        return resp

    def repost_object(self, app, message, post_id):
        '''
        this method create a repost
        '''
        graph = facebook.GraphAPI(access_token=app.env.token)
        share_post = 'https://www.facebook.com/{}/posts/{}'.format(app.env.id, post_id)
        resp = graph.put_object("me",
                                "feed",
                                message=message,
                                link = share_post)
        return resp

    def get_objects(self, app):
        '''
        this method returns the list of posts on the wall
        '''
        graph = facebook.GraphAPI(access_token=app.env.token)

        feed = graph.get_connections("me", "feed")
        posts = feed["data"]

        return posts

    def delete_objects(self, app, id):
        '''
        this method delete a post (repost)
        '''
        graph = facebook.GraphAPI(access_token=app.env.token)

        resp = graph.delete_object(id=id)

        return resp






