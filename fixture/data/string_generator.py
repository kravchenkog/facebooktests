import random
import string

class StringGeneratoHelper:

    def get_random_string(self, num):
        string_rand = ''.join(random.choice(string.ascii_lowercase) for i in range(num))
        return string_rand

