from fixture.environment import Environment
from fixture.data.string_generator import StringGeneratoHelper
from fixture.main_helper import GeneralHelper

class AppManager:

    def __init__(self, env):
        self.env = Environment(env)
        self.stg_gen = StringGeneratoHelper()
        self.gen = GeneralHelper()

